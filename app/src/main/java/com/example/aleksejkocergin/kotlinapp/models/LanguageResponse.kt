package com.example.aleksejkocergin.kotlinapp.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Language(
        @SerializedName("language") @Expose val language: String,
        @SerializedName("confidence") @Expose val confidence: Double
)

data class Result(
        @SerializedName("languages") @Expose val items: List<Language>
)