package com.example.aleksejkocergin.kotlinapp.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.aleksejkocergin.kotlinapp.retrofit.ServiceGeneratorProvider
import com.example.aleksejkocergin.kotlinapp.retrofit.WatsonApiService
import com.example.aleksejkocergin.kotlinapp.ui.views.INewText
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

@InjectViewState
class NewTextPresenter : MvpPresenter<INewText>() {

    private val username: String = "1af1bba3-3ece-4fd9-84c2-a408282c7cfe"
    private val password: String = "BH5ea3FBHpWF"
    private lateinit var language: String

    fun determineLanguage(userText: String) {

        viewState.showProgress()

        val watsonApiService = ServiceGeneratorProvider.provideServiceGenerator()
                .createService(WatsonApiService::class.java, username, password)

        watsonApiService.getLanguage(userText)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe( { result ->
                    language = result.items[0].language
                    showLanguage(Locale(language).displayName)
                    viewState.hideProgress()
                }, { error ->
                    viewState.showMessage(error.message.toString())
                    viewState.hideProgress()
                })
    }

    private fun showLanguage(language: String) {
        viewState.showMessage(language)
    }

    fun hideDialog() {
        viewState.hideMessage()
    }
}